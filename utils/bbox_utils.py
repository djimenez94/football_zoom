def get_bbox_center(bbox):
    x1, y1, x2, y2 = bbox
    return int((x1+x2)/2), int((y1+y2)/2)

def get_bbox_shape(bbox):
    x1, y1, x2, y2 = bbox
    #width, height
    return int(x2-x1), int(y2-y1)

def get_2p_distance(p1, p2):
    return ((p1[0]-p2[0])**2 + (p1[1]-p2[1])**2)**0.5

def is_player_close_to_ball(player_bbox, ball_bbox):
    max_distance_to_ball = 100 
    ball_pos = get_bbox_center(ball_bbox)

    distance_left_foot  = get_2p_distance((player_bbox[0], player_bbox[-1]), ball_pos)
    distance_right_foot = get_2p_distance((player_bbox[2], player_bbox[-1]), ball_pos)
    min_distance = min(distance_left_foot, distance_right_foot)

    return min_distance <= max_distance_to_ball

def is_a_ball(bbox):
    width, height = get_bbox_shape(bbox)
    return (height - width) < 10 
