from ultralytics import YOLO
import supervision as sv
import pickle
import os
import sys
import cv2
import pandas as pd
sys.path.append('../')
from utils import get_bbox_center, get_bbox_shape, is_player_close_to_ball, is_a_ball

class Tracker:
    def __init__(self, model_path):
        self.model = YOLO(model_path)
        self.tracker = sv.ByteTrack(
            track_thresh=0.1,  # Umbral de similitud para asociar detecciones a tracks
            track_buffer=400,   # Número de frames para mantener tracks inactivos
            match_thresh=0.9,  # Umbral de similitud para considerar una detección como match
        )

    def interpolate_ball(self, ball_pos):
        ball_pos_list = []
        for ball_dict in ball_pos:
            if ball_dict and is_a_ball(ball_dict[1].get('bbox')):
                ball_pos_list.append(ball_dict[1].get('bbox'))
            else:
                ball_pos_list.append([])
            
        pd_ball_pos = pd.DataFrame(ball_pos_list, columns=['x1', 'y1', 'x2', 'y2'])

        pd_ball_pos = pd_ball_pos.interpolate()
        pd_ball_pos = pd_ball_pos.bfill()

        ball_pos = [{1: {'bbox': x}} for x in pd_ball_pos.to_numpy().tolist()]

        return ball_pos

    def detect_frames(self, frames):
        batchSize = 15 
        detections = []

        for i in range(0, len(frames),batchSize):
            detectionsBatch = self.model.predict(frames[i:i+batchSize], conf=0.6)
            detections += detectionsBatch
        return detections

    def get_object_tracks(self, frames, readPath=None):
        if readPath is not None and os.path.exists(readPath):
            with open(readPath, 'rb') as f:
                tracks = pickle.load(f)
            return tracks

        detections = self.detect_frames(frames)

        tracks = {
            "players": [],
            "ball": []
        }
        ballTotalFrames = 0
        for frameIdx, detection in enumerate(detections):
            #convert detection to SV detections
            detectionSv = sv.Detections.from_ultralytics(detection)
            ballClass   = 0
            playerClass = 1

            #track objects - adds track id
            detectionSvWithIds = self.tracker.update_with_detections(detectionSv)

            tracks["players"].append({})
            tracks["ball"].append({})

            #we can iterate overdetections - it will take the same position for each array
            for frame in detectionSvWithIds:
                bbox = frame[0].tolist()
                classId = frame[3]
                trackId = frame[4]

                if classId == playerClass:
                    tracks["players"][frameIdx][trackId] = {"bbox":bbox}

            # when we add the track we miss a lot of ball detections so better to use detections previous adding track ids.
            for frame in detectionSv:
                bbox = frame[0].tolist()
                classId = frame[3]

                if classId == ballClass:
                    tracks["ball"][frameIdx][1] = {"bbox":bbox}
                    ballTotalFrames += 1

        if readPath is not None:
            with open(readPath, 'wb') as f:
                pickle.dump(tracks, f)

        return tracks

    def apply_zoom(self, frame, bbox, scale=3):
        # Calcular el centro y tamaño del cuadro delimitador
        xCenter, yCenter = get_bbox_center(bbox)
        width, _ = get_bbox_shape(bbox)

        aspect_ratio = frame.shape[1] / frame.shape[0]
        new_width = int(max(width * scale, frame.shape[1] / scale))
        new_height = int(new_width / aspect_ratio)

        # Asegurar que el nuevo recorte no exceda los límites del fotograma
        new_x1 = max(0, xCenter - new_width // 2)
        new_y1 = max(0, yCenter - new_height // 2)
        new_x2 = min(frame.shape[1], new_x1 + new_width)
        new_y2 = min(frame.shape[0], new_y1 + new_height)

        # Ajustar las coordenadas si el recorte es más pequeño que el área de zoom calculada
        if new_x2 - new_x1 < new_width:
            new_x1 = max(0, new_x2 - new_width)
        if new_y2 - new_y1 < new_height:
            new_y1 = max(0, new_y2 - new_height)

        # Aplicar el corte con las nuevas coordenadas
        zoomed_frame = frame[new_y1:new_y2, new_x1:new_x2]

        # Redimensionar de vuelta al tamaño original si es necesario
        zoomed_frame = cv2.resize(zoomed_frame, (frame.shape[1], frame.shape[0]))

        return zoomed_frame

    def draw_ellipse(self, frame, bbox, color, trackId=None):
        y2 = int(bbox[3])
        xCenter, _  = get_bbox_center(bbox)
        boxWidth, _ = get_bbox_shape(bbox)

        cv2.ellipse(
            frame,
            center=(xCenter,y2),
            axes=(boxWidth, int(0.25 * boxWidth)),
            angle=0.0,
            startAngle=-45,
            endAngle=235,
            color=color,
            thickness=2,
            lineType=cv2.LINE_4
        )

        yPos = 20 + int(0.25 * boxWidth) + y2

        if trackId is not None:
            cv2.putText(frame, str(trackId), (xCenter, yPos), cv2.FONT_HERSHEY_SIMPLEX, 0.6, color, 2)

        return frame

    def draw_annotations(self, frames, tracks, selected_player=-1):
        outputFrames = []
        zoom = 1.0
        previousPlayerBbox = None
        for frameIdx, frame in enumerate(frames):
            frame = frame.copy()

            ballDict   = tracks["ball"][frameIdx]
            playerDict = tracks["players"][frameIdx]

            #draw players ellipse
            for trackId, player in playerDict.items():
                frame = self.draw_ellipse(frame, player["bbox"], (4,15,146), trackId)

            for trackId, ball in ballDict.items():
                frame = self.draw_ellipse(frame, ball["bbox"], (146,15,4))

            if selected_player in playerDict.keys():
                previousPlayerBbox = playerDict[selected_player]["bbox"]
                if is_player_close_to_ball(playerDict[selected_player]["bbox"], ballDict[1]["bbox"]):
                    zoom += 0.15 
                    if zoom > 3.0: zoom = 3.0
                else:
                    zoom -= 0.15
                    if zoom < 1.0 : zoom = 1.0 

                if zoom > 1.0:
                    frame = self.apply_zoom(frame, playerDict[selected_player]["bbox"], scale=zoom)
            else:
                if previousPlayerBbox != None and zoom > 1.0:
                    frame = self.apply_zoom(frame, previousPlayerBbox, scale=zoom)

            outputFrames.append(frame)
        return outputFrames
