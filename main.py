from utils import read_video, save_video
from tracker import Tracker
import os

def extract_filename(path):
    return os.path.splitext(os.path.basename(path))[0]

def main():
    #read video
    path = 'input_videos/08fd33_4.mp4'
    video = read_video(path)

    tracker  = Tracker('models/best.pt')
    filename = extract_filename(path)
    pklFile  = f'stubs/tracks_{filename}.pkl'
    tracks   = tracker.get_object_tracks(video, pklFile)

    tracks["ball"] = tracker.interpolate_ball(tracks["ball"])

    #Draw ellipse 
    selected_player_id = -1
    outputVideo = tracker.draw_annotations(video, tracks, selected_player_id)

    #save video
    output_video_path = f'output_videos/output_{filename}.avi'
    save_video(outputVideo, output_video_path)

if __name__ == '__main__':
    main()