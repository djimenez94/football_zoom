# football_zoom

This project is part of master's final work.

![Ejemplo de GIF](https://gitlab.com/djimenez94/football_zoom/-/raw/main/video_test.gif)


[MP4 Video](https://gitlab.com/djimenez94/football_zoom/-/raw/main/test_video.mp4)

## Dataset

The dataset generated is in YOLO format. It is compressed in `dataset_generated.zip`.

There are 3 classes:
- ball
- player
- refree

## Run the project
In order to run the project it is recommended to place your videos in `input_videos` folder.

There are different steps needed in order to run the project properly.
1. Create 2 folders: `stubs` and `output_videos` (in the main folder)
   ```
   mkdir stubs
   mkdir output_videos
   ```
2. Modify the `path` in the `main.py` file.
   ```python
   def main():
    #read video
    path = 'input_videos/08fd33_4.mp4'
    video = read_video(path)
   ```
3. Run the project `python3 .\main.py`
4. Look at the video generated and modify the `main.py` to follow the player (track id) you want. The value to modify is `selected_player_id`.
   ```python
    #Draw ellipse 
    selected_player_id = -1
    outputVideo = tracker.draw_annotations(video, tracks, selected_player_id)
   ```
5. Run the project again.

